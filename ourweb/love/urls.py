from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.loveindex, name='loveindex'),
    url(r'^gallery/', views.gallery, name='gallery'),

]

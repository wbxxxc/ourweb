from django.shortcuts import render

# Create your views here.


def loveindex(request):
    return render(request, 'love/index.html')


def gallery(request):
    return render(request, 'love/gallery.html')

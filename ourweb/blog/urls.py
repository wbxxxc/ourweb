from django.conf.urls import url
from . import views

app_name = 'blog'
# 视图函数命名空间，解决复杂的 Django 项目有许多视图函数造成的冲突

urlpatterns = [
	# url(r'^$', views.blogindex, name='blogindex'),
	url(r'^login/$', views.bloglogin, name='bloglogin'),
	url(r'^regist/$', views.blogregist, name='blogregist'),
	url(r'^write/$', views.blogwrite, name='blogwrite'),
	url(r'^post/(?P<pk>[0-9]+)/$', views.detail, name='detail'),
	url(r'^archives/(?P<year>[0-9]{4})/(?P<month>[0-9]{1,2})/$', views.archives, name='archives'),
	# url(r'^category/(?P<pk>[0-9]+)/$', views.category, name='category'),
	# 调用类视图函数（类视图转换成函数视图），
	url(r'^$', views.BlogIndexView.as_view(), name='blogindex'),
	url(r'^category/(?P<pk>[0-9]+)/$', views.CategoryView.as_view(), name='category'),
	url(r'^tag/(?P<pk>[0-9]+)/$', views.TagView.as_view(), name='tag'),
	url(r'^search/$', views.search, name='search'),
	
]
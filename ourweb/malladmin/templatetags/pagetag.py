from django import template
register = template.Library()

# 自定义过滤器
@register.filter
def kong_upper(val):
    # print ('val from template:',val)
    return val.upper()

# 自定义分页优化标签
from django.utils.html import format_html
@register.simple_tag
def showpage(count,request):
    # 获取当前页码数p,如果没有缺省为1
    p = int(request.GET.get('p',1))
    # 要显示的起始页和结束页（显示10页，当前页，当前页的前4页和后5页）
    start = p - 4
    end = p + 5
    # 判断页码对于总页数的位置
    # 如果总页数小于10页
    if count < 10:
        start = 1
        end = count
    # 如果当前页在总页数的最后5页之内
    elif p > count-5:
        start = count - 9
        end = count
    # 如果当前页在总页数的最前4页之内
    elif p < 5:
        start = 1
        end = 10

    # 获取当前请求的url
    url = request.path
    # 获取当前请求中的参数 
    # <QueryDict: {'p': ['26'], 'b': ['2'], 'a': ['1']}>
    # &a=1&b=2
    # 去除参数中的p
    args = ''
    for k,v in request.GET.items():
        if k != 'p':
            args += '&'+k+'='+v

    # 拼接HTML字符串 
    s = ''

    # 首页
    s += '<li><a href="{url}?p={v}{args}">首页</a></li>'.format(v=1,url=url,args=args)

    # 上一页，带判断，是否是第一页
    # 如果是第一页，不显示上一页
    if p != 1:
        s += '<li><a href="{url}?p={v}{args}">上一页</a></li>'.format(v=p-1,url=url,args=args)
    # 始终显示上一页
    # if p == 1:
    #     s += '<li><a href="{url}?p={v}{args}">上一页</a></li>'.format(v=1,url=url,args=args)
    # else:
    #     s += '<li><a href="{url}?p={v}{args}">上一页</a></li>'.format(v=p-1,url=url,args=args)

    # 循环页码数
    for x in range(start,end+1):
        # 给当前页的页码加高亮显示的类
        if x == p:
            s += '<li class="am-active"><a href="{url}?p={v}{args}">{v}</a></li>'.format(v=x,url=url,args=args)
        else:
            s += '<li ><a href="{url}?p={v}{args}">{v}</a></li>'.format(v=x,url=url,args=args)

    # 下一页，带判断，是否最后一页
    # 如果是最后一页，不显示下一页
    if p != count:
        s += '<li><a href="{url}?p={v}{args}">下一页</a></li>'.format(v=p+1,url=url,args=args)
    # 始终显示下一页
    # if p == count:
        # s += '<li><a href="{url}?p={v}{args}">下一页</a></li>'.format(v=count,url=url,args=args)
    # else:
        # s += '<li><a href="{url}?p={v}{args}">下一页</a></li>'.format(v=p+1,url=url,args=args)
    # 尾页
    s += '<li><a href="{url}?p={v}{args}">尾页</a></li>'.format(v=count,url=url,args=args)

    # 总页数
    s += '<li>共{v}页</li>'.format(v=count)

    return format_html(s)


# 改版
# @register.simple_tag
# def circle_page(curr_page,loop_page,url):
#     offset = abs(curr_page - loop_page)
#     if offset < 3:
#         # if curr_page == loop_page:
#         #     page_ele = '<li class="am-active"><a href="%s%s?%s">%s</a></li>'%(url,loop_page,where,loop_page)
#         #     # page_ele = str(loop_page)
#         # else:
#         #     page_ele = '<li><a href="%s%s?%s">%s</a></li>'%(url,loop_page,where,loop_page)
#         #     # page_ele = str(loop_page)
#         if curr_page == loop_page:
#             page_ele = '<li class="am-active"><a href="{url}?p={p}">{p}</a></li>'.format(url=url,p=loop_page)
#             return format_html(page_ele)

#         page_ele = '<li><a href="{url}?p={p}">{p}</a></li>'.format(url=url,p=loop_page)
#         return format_html(page_ele)
#     else:
#         return ''

# 原版
# @register.simple_tag
# def circle_page(curr_page,loop_page,url,where):
#     offset = abs(curr_page - loop_page)
#     if offset < 3:
#         if curr_page == loop_page:
#             page_ele = '<li class="am-active"><a href="%s%s?%s">%s</a></li>'%(url,loop_page,where,loop_page)
#             # page_ele = str(loop_page)
#         else:
#             page_ele = '<li><a href="%s%s?%s">%s</a></li>'%(url,loop_page,where,loop_page)
#             # page_ele = str(loop_page)
#         return format_html(page_ele)
#     else:
#         return ''
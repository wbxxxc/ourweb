from django.db import models

# Create your models here.


# 用户表
class Users(models.Model):
    # 账号，唯一，不为空
    username = models.CharField(max_length=32, unique=True, null=True)
    # 密码，不为空
    password = models.CharField(max_length=128, null=True)
    # Email，不为空
    email = models.CharField(max_length=64, null=True)
    # 收货人
    tname = models.CharField(max_length=32)
    # 性别，1男0女2未公开
    sex = models.IntegerField(default=2)
    # 电话
    phone = models.CharField(max_length=16)
    # 地址
    address = models.CharField(max_length=128)
    # 状态，默认1，1启用2禁用0管理员
    state = models.IntegerField(default=1)
    # 注册时间，自动生成
    addtime = models.DateTimeField(auto_now_add=True)
    # 用户头像，路径
    pic = models.CharField(max_length=128)


# 商品分类表
class Classify(models.Model):
    name = models.CharField(max_length=32)
    pid = models.IntegerField()
    path = models.CharField(max_length=32)


# 商品表
class Goods(models.Model):
    # 商品分类id，对Classify表的id的外键
    typeid = models.ForeignKey(to='Classify', to_field='id')
    # 商品名称
    gname = models.CharField(max_length=64)
    # 价格
    price = models.DecimalField(max_digits=8, decimal_places=2)
    # 图片
    gpic = models.CharField(max_length=128)
    # 信息，简介
    ginfo = models.TextField()
    # 状态，1新添加2在售3下架
    state = models.IntegerField()
    # 库存量
    store = models.IntegerField()
    # 添加时间
    addtime = models.DateTimeField(auto_now_add=True)


# 订单表
class Order(models.Model):
    # 用户id，对Users表的id的外键
    uid = models.ForeignKey(to='Users', to_field="id")
    # 收货人
    addname = models.CharField(max_length=50)
    # 收货地址
    address = models.CharField(max_length=255)
    # 收货电话
    addphone = models.CharField(max_length=11)
    # 邮编
    addcode = models.CharField(max_length=10)
    # 订单总价
    totalprice = models.FloatField()
    # 订单总商品数
    totalnum = models.IntegerField()
    # 订单状态，1未付款2已付款,待发货3已发货,待收货4已完成5已取消
    status = models.IntegerField()
    # 订单创建时间
    addtime = models.DateTimeField(auto_now_add=True)


# 订单详情
class OrderInfo(models.Model):
    # 订单号，对订单表的id的外键
    orderid = models.ForeignKey(to='Order', to_field="id")
    # 用户id，对用户表的id的外键
    gid =  models.ForeignKey(to='Goods', to_field="id")
    # 商品总数
    num = models.IntegerField()
    # 商品总价
    price = models.FloatField()

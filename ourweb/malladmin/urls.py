from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.indexadmin, name='indexadmin'),
    # -----------users---------------
    url(r'^userlist/', views.userlist, name='userlist'),
    # url(r'^userlist/(?P<pIndex>[0-9]+)', views.userlist, name='userlist'),
    url(r'^useradd/', views.useradd, name='useradd'),
    url(r'^userinsert/', views.userinsert, name='userinsert'),
    url(r'^useredit/(?P<uid>[0-9]+)', views.useredit, name='useredit'),
    url(r'^userupdate/', views.userupdate, name='userupdate'),
    url(r'^userdel/(?P<uid>[0-9]+)', views.userdel, name='userdel'),
    url(r'^changepass/(?P<uid>[0-9]+)', views.changepass, name='changepass'),
    # url(r'^changepic/(?P<uid>[0-9]+)', views.changepic, name='changepic'),
    # url(r'^htx/', views.huantouxiang, name='htx'),
    # -----------classify---------------
    url(r'^classifylist/', views.classifylist, name='classifylist'),
    url(r'^classifyadd/', views.classifyadd, name='classifyadd'),
    url(r'^classifyinsert/', views.classifyinsert, name='classifyinsert'),
    url(r'^classifyedit/(?P<cid>[0-9]+)', views.classifyedit, name='classifyedit'),
    url(r'^classifyupdate/', views.classifyupdate, name='classifyupdate'),
    url(r'^classifydel/(?P<cid>[0-9]+)', views.classifydel, name='classifydel'),
    # -----------goods--------------------
    url(r'^goodslist/', views.goodslist, name='goodslist'),
    url(r'^goodsadd/', views.goodsadd, name='goodsadd'),
    url(r'^goodsinsert/', views.goodsinsert, name='goodsinsert'),
    url(r'^goodsedit/(?P<gid>[0-9]+)', views.goodsedit, name='goodsedit'),
    url(r'^goodsupdate/', views.goodsupdate, name='goodsupdate'),
    url(r'^goodsdel/(?P<gid>[0-9]+)', views.goodsdel, name='goodsdel'),
    url(r'^goodsclass/(?P<cid>[0-9]+)', views.goodsclass, name='goodsclass'),
    # ------------login------------------------
    url(r'^login/', views.login, name="login"),
    url(r'^dologin/', views.dologin, name="dologin"),
    url(r'^logout/', views.logout, name="logout"),
    url(r'^verifycode/', views.verifycode, name='verifycode'),
    # ------------orders-----------------------
    url(r'^orders/', views.orders, name='orders'),
    url(r'^kuaidi/(?P<oid>[0-9]+)', views.kuaidi, name='kuaidi'),
    url(r'^xiangqing/(?P<oid>[0-9]+)', views.xiangqing, name='orderxiangqing'),
]

from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.hashers import make_password, check_password
from django.core.paginator import Paginator
from django.core import serializers
from .models import Users, Classify, Goods, Order, OrderInfo
import os, time, copy

def indexadmin(request):
    return render(request, 'malladmin/indexadmin.html')


def userlist(request):
    # vips = Users.objects.all()
    # 此视图包括查询全部用户(列表首页默认)和
    # 按照条件筛选用户查询(搜索框，搜索条件searchusers和关键字kws两个参数)

    vips = Users.objects.all().order_by('id')

    # 获取请求的搜索条件和关键字
    tiaojian = request.GET.get('searchusers', None)
    gjz = request.GET.get('kws', '')
    # 判断查询条件，执行相应查询操作，vips重新赋值，如果没有搜索条件就按查询全部
    if tiaojian == 'username':
        # filter过滤器搜索，__contains是模糊搜索，搜索包含关键字的数据
        vips = Users.objects.filter(username__contains=gjz).order_by('id')
    elif tiaojian == 'tname':
        vips = Users.objects.filter(tname__contains=gjz).order_by('id')
    elif tiaojian == 'phone':
        vips = Users.objects.filter(phone__contains=gjz).order_by('id')
    elif tiaojian == 'sex':
        if gjz == '男':
            vips = Users.objects.filter(sex=1).order_by('id')
        elif gjz == '女':
            vips = Users.objects.filter(sex=0).order_by('id')
        elif gjz == '未公开':
            vips = Users.objects.filter(sex=2).order_by('id')
    elif tiaojian == 'state':
        if gjz == '启用':
            vips = Users.objects.filter(state=1).order_by('id')
        elif gjz == '禁用':
            vips = Users.objects.filter(state=2).order_by('id')
        elif gjz == '管理员':
            vips = Users.objects.filter(state=0).order_by('id')

    # 实例化分页类
    paginator = Paginator(vips, 5)
    # 获取当前页码
    p = int(request.GET.get('p', 1))
    # 获取分页数据对象[{id:1},{id:2}]  [{id:3},{id:4}]
    viplist = paginator.page(p)

    return render(request, 'malladmin/userlist.html', {'userinfo': viplist, 'p': p})


def useradd(request):
    return render(request, 'malladmin/useradd.html')


def userinsert(request):
    try:
        vip = Users()
        vip.username = request.POST['username']
        vip.password = make_password(request.POST['password'], None, 'pbkdf2_sha256')
        vip.email = request.POST['email']
        vip.tname = request.POST['tname']
        vip.sex = request.POST['sex']
        vip.phone = request.POST['phone']
        vip.address = request.POST['address']
        vip.state = request.POST['state']
        vip.pic = uploadpic(request)
        vip.save()
        return HttpResponse('<script>alert("添加成功");location.href="/malladmin/userlist/";</script>')
    except:
        return HttpResponse('<script>alert("添加失败");location.href="/malladmin/useradd/";</script>')


def useredit(request, uid):
    vip = Users.objects.get(id=uid)
    return render(request, 'malladmin/useredit.html', {'userinfo': vip})


def userupdate(request):
    try:
        vip = Users.objects.get(id=request.POST['uid'])
        vip.username = request.POST['username']
        vip.email = request.POST['email']
        vip.tname = request.POST['tname']
        vip.sex = request.POST['sex']
        vip.phone = request.POST['phone']
        vip.address = request.POST['address']
        vip.state = request.POST['state']

        newpic = request.FILES.get('pic', None)
        if newpic:
            picname = str(time.time()) + "." + newpic.name.split('.').pop()
            destination = open("./static/userpics/" + picname, "wb+")
            for chunk in newpic.chunks():  # 分块写入文件  
                destination.write(chunk)
            destination.close()

            if vip.pic != "/static/userpics/default.jpg":
                os.remove('.' + vip.pic)
            vip.pic = '/static/userpics/' + picname
        vip.save()
        return HttpResponse('<script>alert("修改成功");location.href="/malladmin/userlist/";</script>')
    except:
        return HttpResponse('<script>alert("修改失败");location.href="/malladmin/useradd/";</script>')


def userdel(request, uid):
    vip = Users.objects.get(id=uid)
    if vip.pic == "/static/userpics/default.jpg":
        vip.delete()
    else:
        os.remove('.' + vip.pic)
        vip.delete()
    return HttpResponse('<script>alert("用户已删除");location.href="/malladmin/userlist/";</script>')


def changepass(request, uid):
    vip = Users.objects.get(id=uid)
    return render(request, 'malladmin/userchangepass.html', {'userinfo': vip})


# def changepic(request, uid):
# 	vip = Users.objects.get(id=uid)
# 	return render(request, 'malladmin/userchangepic.html', {'userinfo':vip})

# def huantouxiang(request):
# 	vip = Users.objects.get(id=request.POST['uid'])

# 	newpic = request.FILES.get('pic', None)
# 	if newpic:
# 		picname = str(time.time())+"."+newpic.name.split('.').pop()
# 		destination = open("./static/userpics/"+picname,"wb+")
# 		for chunk in newpic.chunks():      # 分块写入文件  
# 			destination.write(chunk)  
# 		destination.close()

# 		if vip.pic != "/static/userpics/default.jpg":
# 			os.remove('.'+vip.pic)
# 		vip.pic = '/static/userpics/'+picname
# 		vip.save()

# 		return HttpResponse('<script>alert("头像已焕然一新");location.href="/malladmin/userlist/"</script>')

def uploadpic(request):
    userpic = request.FILES.get("pic", None)
    if not userpic:
        return "/static/userpics/default.jpg"
    picname = str(time.time()) + "." + userpic.name.split('.').pop()
    destination = open("./static/userpics/" + picname, "wb+")
    for chunk in userpic.chunks():  # 分块写入文件  
        destination.write(chunk)
    destination.close()
    picname = '/static/userpics/' + picname
    return picname


# --------------商品分类Classify---------

def classifylist(request):
    # 使用框架的extra方法和mysql的concat，多字段搜索并拼接字符串
    # fenlei = Classify.objects.extra(select = {'paths':'concat(path,id)'}).order_by('paths')
    # 循环重写name字段，拼接制表符优化显示，
    # for i in fenlei:
    # 	n = len(i.path) - 2
    # 	i.name = (n*'|______')+i.name
    fenlei = chafenlei(request)

    # 获取请求的搜索条件和关键字
    tiaojian = request.GET.get('searchclassify', None)
    gjz = request.GET.get('kws', '')
    # 判断查询条件，执行相应查询操作，vips重新赋值，如果没有搜索条件就按查询全部
    if tiaojian == 'name':
        # filter过滤器搜索，__contains是模糊搜索，搜索包含关键字的数据
        fenlei = Classify.objects.filter(name__contains=gjz).order_by('id')
    elif tiaojian == 'cid':
        fenlei = Classify.objects.filter(id=gjz).order_by('id')

    # 实例化分页类
    paginator = Paginator(fenlei, 10)
    # 获取当前页码
    p = int(request.GET.get('p', 1))
    # 获取分页数据对象[{id:1},{id:2}]  [{id:3},{id:4}]
    fenleilist = paginator.page(p)

    return render(request, 'malladmin/classifylist.html', {'classifyinfo': fenleilist, 'p': p})


def classifyadd(request):
    fenlei = chafenlei(request)
    return render(request, 'malladmin/classifyadd.html', {'classifyinfo': fenlei})


def classifyinsert(request):
    try:
        new = Classify()
        new.name = request.POST['name']
        new.pid = request.POST['pid']
        # 判断是否一级分类，定义path
        if new.pid == '0':
            new.path = '0,'
        else:
            father = Classify.objects.get(id=new.pid)
            new.path = father.path + new.pid + ','
        new.save()
        return HttpResponse('<script>alert("添加成功");location.href="/malladmin/classifylist/"</script>')
    except:
        return HttpResponse('<script>alert("添加失败");location.href="/malladmin/classifyadd/"</script>')


def classifyedit(request, cid):
    fenlei = chafenlei(request)
    selected = Classify.objects.get(id=cid)
    return render(request, 'malladmin/classifyedit.html', {'classifyinfo': fenlei, 'sd': selected})


def classifyupdate(request):
    gai = Classify.objects.get(id=request.POST['cid'])
    gai.name = request.POST['name']
    gai.save()
    return HttpResponse('<script>alert("修改成功");location.href="/malladmin/classifylist/"</script>')


def classifydel(request, cid):
    son = Classify.objects.filter(pid=cid).count()
    if son:
        return HttpResponse('<script>alert("该分类下仍有子类，不能删除");location.href="/malladmin/classifylist/"</script>')
    else:
        fenlei = Classify.objects.get(id=cid)
        # 利用外键，获取当前分类下的商品
        son_g = fenlei.goods_set.all().count()
        # 如果分类下有商品不能删
        if son_g:
            return HttpResponse('<script>alert("该分类下仍有商品在售，不能删除");location.href="/malladmin/classifylist/"</script>')
        else:
            fenlei.delete()
            return HttpResponse('<script>alert("分类已删除");location.href="/malladmin/classifylist/"</script>')


def chafenlei(request):
    # 使用框架的extra方法和mysql的concat，多字段搜索并拼接字符串
    fenlei = Classify.objects.extra(select={'paths': 'concat(path,id)'}).order_by('paths')
    # 循环重写name字段，拼接制表符优化显示，
    for i in fenlei:
        n = len(i.path) - 2
        i.name = (n * '|____') + i.name
    return fenlei


# ---------------Goods----------------------
def goodslist(request):
    gs = Goods.objects.all().order_by('id')

    fenlei = Classify.objects.all()
    fl = []
    for i in fenlei:
        a = i.path.count(',')
        if a > 1:
            fl.append(i)

    # 获取请求的搜索条件和关键字
    tiaojian = request.GET.get('searchcg', None)
    if tiaojian:
        gs = Goods.objects.filter(typeid=tiaojian)

    tiaojian = request.GET.get('searchgoods', None)
    gjz = request.GET.get('kws', '')
    # 判断查询条件，执行相应查询操作，gs重新赋值，如果没有搜索条件就按查询全部
    if tiaojian == 'gname':
        # filter过滤器搜索，__contains是模糊搜索，搜索包含关键字的数据
        gs = Goods.objects.filter(gname__contains=gjz).order_by('id')
    elif tiaojian == 'state':
        if gjz == '新品':
            gs = Goods.objects.filter(state=1).order_by('id')
        elif gjz == '在售':
            gs = Goods.objects.filter(state=2).order_by('id')
        elif gjz == '下架':
            gs = Goods.objects.filter(state=3).order_by('id')

    # 实例化分页类
    paginator = Paginator(gs, 5)
    # 获取当前页码
    p = int(request.GET.get('p', 1))
    # 获取分页数据对象[{id:1},{id:2}]  [{id:3},{id:4}]
    gsl = paginator.page(p)
    return render(request, 'malladmin/goodslist.html', {'goodsinfo': gsl, 'p': p, 'classinfo': fl})


def goodsadd(request):
    fenlei = Classify.objects.all()
    fl = []
    for i in fenlei:
        a = i.path.count(',')
        if a > 1:
            fl.append(i)

    return render(request, 'malladmin/goodsadd.html', {'classinfo': fl})


def goodsinsert(request):
    father = Classify.objects.get(id=request.POST['cid'])
    try:
        gg = Goods()
        gg.typeid = father
        gg.gname = request.POST['gname']
        gg.price = request.POST['price']
        gg.store = request.POST['store']
        gg.state = request.POST['state']
        gg.gpic = uploadimg(request)
        gg.ginfo = request.POST['ginfo']
        gg.save()
        return HttpResponse('<script>alert("商品添加成功");location.href="/malladmin/goodsadd/"</script>')
    except:
        return HttpResponse('<script>alert("商品添加失败!!");location.href="/malladmin/goodsadd/"</script>')


def goodsedit(request, gid):
    fenlei = Classify.objects.all()
    fl = []
    for i in fenlei:
        a = i.path.count(',')
        if a > 1:
            fl.append(i)
    gs = Goods.objects.get(id=gid)

    return render(request, 'malladmin/goodsedit.html', {'classinfo': fl, 'goodsinfo': gs})


def goodsupdate(request):
    gg = Goods.objects.get(id=request.POST['gid'])
    gg.gname = request.POST['gname']
    gg.price = request.POST['price']
    gg.store = request.POST['store']
    gg.state = request.POST['state']
    gg.ginfo = request.POST['ginfo']
    newgpic = request.FILES.get('gpic', None)
    if newgpic:
        picname = str(time.time()) + "." + newgpic.name.split('.').pop()
        destination = open("./static/goodsimg/" + picname, "wb+")
        for chunk in newgpic.chunks():  # 分块写入文件  
            destination.write(chunk)
        destination.close()

        if gg.gpic != "/static/goodsimg/default.jpg":
            os.remove('.' + gg.gpic)
        gg.gpic = '/static/goodsimg/' + picname
    gg.save()
    return HttpResponse('<script>alert("修改成功");location.href="/malladmin/goodslist/";</script>')


def goodsdel(request, gid):
    gg = Goods.objects.get(id=gid)
    if gg.state == 3:
        if gg.gpic == "/static/goodsimg/default.jpg":
            gg.delete()
        else:
            os.remove('.' + gg.gpic)
            gg.delete()
        return HttpResponse('<script>alert("商品已删除");location.href="/malladmin/goodslist/";</script>')
    else:
        return HttpResponse('<script>alert("商品在售，未下架不能删除");location.href="/malladmin/goodslist/";</script>')


def goodsclass(request, cid):
    clist = Classify.objects.filter(pid=cid).order_by('id')
    a = serializers.serialize('json', clist)
    return HttpResponse(a)


def uploadimg(request):
    goodspic = request.FILES.get("gpic", None)
    if not goodspic:
        return "/static/goodsimg/default.jpg"
    picname = str(time.time()) + "." + goodspic.name.split('.').pop()
    destination = open("./static/goodsimg/" + picname, "wb+")
    for chunk in goodspic.chunks():  # 分块写入文件  
        destination.write(chunk)
    destination.close()
    picname = '/static/goodsimg/' + picname
    return picname


# --------------login----------------------
def login(request):
    return render(request, 'malladmin/login.html')


def dologin(request):
    # 判断验证码是否正确
    if request.POST['vcode'].upper() != request.session['verifycode']:
        return HttpResponse('<script>alert("验证码错误");location.href="/malladmin/login/"</script>')
    try:
        # 获取用户是否存在
        ob = Users.objects.get(username=request.POST['username'])
        # 判断是否为管理员
        if ob.state == 0:
            # 验证密码
            res = check_password(request.POST['password'], ob.password)
            # 判断密码是否正确
            if res:
                # 登录成功,用户信息.,记录到session,跳转地址
                request.session['MalladminLogin'] = {'uid': ob.id, 'uname': ob.username, 'upic': ob.pic}
                return HttpResponse('<script>alert("登录成功");location.href="/malladmin/"</script>')
            else:
                # 密码错误
                # raise
                return HttpResponse('<script>alert("用户名或密码不正确");location.href="/malladmin/login/"</script>')
        else:
            # 不是管理员
            # raise
            return HttpResponse('<script>alert("用户名或密码不正确");location.href="/malladmin/login/"</script>')
    except:
        # 用户不存在
        return HttpResponse('<script>alert("用户名或密码不正确");location.href="/malladmin/login/"</script>')


def logout(request):
    # 清除session登录信息
    request.session['MyadminLogin'] = {}
    return HttpResponse('<script>location.href="/malladmin/login/"</script>')


def verifycode(request):
    # 引入绘图模块
    from PIL import Image, ImageDraw, ImageFont
    # 引入随机函数模块
    import random
    # 引入数据流模块
    import io
    # 定义变量，用于画面的背景色、宽、高
    bgcolor = (random.randrange(20, 100), random.randrange(20, 100), 255)
    width = 100
    height = 40
    # 创建画面对象
    im = Image.new('RGB', (width, height), bgcolor)
    # 创建画笔对象
    draw = ImageDraw.Draw(im)
    # 调用画笔的point()函数绘制噪点
    for i in range(0, 100):
        xy = (random.randrange(0, width), random.randrange(0, height))
        fill = (random.randrange(0, 255), 255, random.randrange(0, 255))
        draw.point(xy, fill=fill)
    # 定义验证码的备选值
    str1 = 'ABCD123EFGHIJK456LMNPQRS789TUVWXYZ'
    # str1 = '123456789'
    # 随机选取4个值作为验证码
    rand_str = ''
    for i in range(0, 4):
        rand_str += str1[random.randrange(0, len(str1))]
    # 构造字体对象
    # font = ImageFont.truetype('NotoSansCJK-Light.ttc', 23) # ubuntu使用,windows下报错,字体文件及路径问题
    # font = ImageFont.truetype(r'C:\Windows\Fonts\Arial.ttf', 23)  # windows使用
    font = ImageFont.truetype('cmr10.ttf', 23)  # centos使用
    # font = ImageFont.load_default().font
    # 构造字体颜色
    fontcolor = (255, random.randrange(0, 255), random.randrange(0, 255))
    # 绘制4个字
    draw.text((5, 2), rand_str[0], font=font, fill=fontcolor)
    draw.text((25, 2), rand_str[1], font=font, fill=fontcolor)
    draw.text((50, 2), rand_str[2], font=font, fill=fontcolor)
    draw.text((75, 2), rand_str[3], font=font, fill=fontcolor)
    # 释放画笔
    del draw
    # 存入session，用于做进一步验证
    request.session['verifycode'] = rand_str
    # 内存文件操作
    buf = io.BytesIO()
    # 将图片保存在内存中，文件类型为png
    im.save(buf, 'png')
    # 将内存中的图片数据返回给客户端，MIME类型为图片png
    return HttpResponse(buf.getvalue(), 'image/png')


# ------------订单管理--------------------
def orders(request):
    # 查询用户的订单
    ob = Order.objects.all()
    # filter 查询集 [{info:[{},{}]},{}]
    for x in ob:
        x.info = x.orderinfo_set.all()
    # print(x.uid)
    # x.buyer = Users.objects.get(id=x.uid)
    # x.buyer = x.buyer['username']
    # print(ob[0].info[0].gid.title)

    # 获取请求的搜索条件和关键字
    tiaojian = request.GET.get('orderstatus', None)
    if tiaojian:
        if tiaojian == '0':
            ob = Order.objects.all()
            for x in ob:
                x.info = x.orderinfo_set.all()
        else:
            ob = Order.objects.filter(status=tiaojian)
            for x in ob:
                x.info = x.orderinfo_set.all()

    tiaojian = request.GET.get('searchorders', None)
    gjz = request.GET.get('kws', '')
    # 判断查询条件，执行相应查询操作，gs重新赋值，如果没有搜索条件就按查询全部
    if tiaojian == 'addname':
        # filter过滤器搜索，__contains是模糊搜索，搜索包含关键字的数据
        ob = Order.objects.filter(addname__contains=gjz).order_by('id')
        for x in ob:
            x.info = x.orderinfo_set.all()
    elif tiaojian == 'addphone':
        ob = Order.objects.filter(addphone=gjz).order_by('id')
        for x in ob:
            x.info = x.orderinfo_set.all()
    elif tiaojian == 'buyer':
        try:
            buyer = Users.objects.get(username=gjz)
            ob = Order.objects.filter(uid=buyer.id).order_by('id')
            for x in ob:
                x.info = x.orderinfo_set.all()
        except:
            ob = Order.objects.all()
            for x in ob:
                x.info = x.orderinfo_set.all()

    # 实例化分页类
    paginator = Paginator(ob, 3)
    # 获取当前页码
    p = int(request.GET.get('p', 1))
    # 获取分页数据对象[{id:1},{id:2}]  [{id:3},{id:4}]
    ob = paginator.page(p)
    return render(request, 'malladmin/orderlist.html', {'orders': ob})


# 卖家发货
def kuaidi(request, oid):
    ob = Order.objects.get(id=oid)
    ob.status = 3
    ob.save()
    return HttpResponse('<script>location.href="/malladmin/orders/?orderstatus=2"</script>')


def xiangqing(request, oid):
    ob = Order.objects.get(id=oid)
    ob.info = ob.orderinfo_set.all()
    return render(request, 'malladmin/orderinfo.html', {'order': ob})


from django.shortcuts import render
from django.http import HttpResponse
import re


class MyadminLoginMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # 检测当前的请求是否已经登录,如果已经登录,则放行,如果未登录,则跳转到登录页
        # 获取当前用户的请求路径  /malladmin/开头  但不是 /malladmin/login/ /malladmin/dologin/ /malladmin/verifycode
        urllist = ['/malladmin/login/','/malladmin/dologin/','/malladmin/verifycode/']
        # 判断是否进入了后台,并且不是进入登录页面
        if re.match('/malladmin/', request.path) and request.path not in urllist:

            # 检测session中是否存在 adminlogin的数据记录
            if not request.session.get('MalladminLogin', False):
                # 如果在session没有记录,则证明没有登录,跳转到登录页面
                return HttpResponse('<script>alert("请先登录");location.href="/malladmin/login/";</script>')

        # 允许:首页,列表,详情,购物车;   不允许:个人中心,下单,付款,我的订单,
        userurllist = ['/mall/confirm/', '/mall/createorder/', '/mall/pay/', '/mall/myorder/']
        if request.path in userurllist:
            if not request.session.get('UserLogin', ''):
                return HttpResponse('<script>alert("请先登录");location.href="/mall/login/";</script>')
        
        response = self.get_response(request)
        return response

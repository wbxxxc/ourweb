from django.apps import AppConfig


class BlogcommentsConfig(AppConfig):
    name = 'blogcomments'

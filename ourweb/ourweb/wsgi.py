"""
WSGI config for ourweb project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os, sys

from django.core.wsgi import get_wsgi_application

# ----add pythonpath to syspath----
sys.path.append('/usr/local/python35/bin/')
# ----add projectpath to syspath----
sys.path.append('/var/www/ourweb/')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ourweb.settings")

application = get_wsgi_application()

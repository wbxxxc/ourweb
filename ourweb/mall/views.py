from django.shortcuts import render

# Create your views here.

from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.hashers import make_password, check_password
from malladmin.models import Users, Classify, Goods, Order, OrderInfo
import os, time


def classlist():
    # 查询所有的二级分类
    cl = Classify.objects.exclude(pid=0)
    return cl


def mallindex(request):
    # 查询所有顶级分类
    t = Classify.objects.filter(pid=0)

    arr = []
    for v in t:
        v.sub = Classify.objects.filter(pid=v.id)
        for x in v.sub:
            x.sub = Goods.objects.filter(typeid_id=x.id)
        arr.append(v)

    # arr = [
    #     {'name':'服装','sub':[{'name':'男装','sub':[{}]},{'name':'女装','sub':[{},{}]}]},
    #     {'name':'数码','sub':[{'name':'手机'}]}
    # ]

    # 手机，二级分类下的商品
    sj = Goods.objects.filter(typeid=5)
    # 手机配件，二级分类下的商品
    sjpj = Goods.objects.filter(typeid=6)
    # 电脑，一级分类，先取下面的二级分类，再取二级分类下的商品
    dn = Classify.objects.get(id=2)
    dn.sub = Classify.objects.filter(pid=2)
    for i in dn.sub:
        i.sub = Goods.objects.filter(typeid_id=i.id)
    print(dn)

    context = {'cl': classlist(), 'cgdata': arr, 'sj': sj, 'sjpj': sjpj, 'dn': dn}

    return render(request, 'mall/index.html', context)


def shoplist(request, cid):
    # 查询当前的分类
    cd = Classify.objects.get(id=cid)

    # 判断当前分类是否为一级分类
    if cd.pid == 0:
        # 如果是一级分类,则再查询当前分类下的子类
        cd.sub = Classify.objects.filter(pid=cid)
        # 再查询子类下的商品信息
        for v in cd.sub:
            v.sub = Goods.objects.filter(typeid_id=v.id)
    else:
        # 二级分类
        # 查询当前分类下的商品
        cd.sub = Goods.objects.filter(typeid_id=cd.id)
        # 查询当前分类下的父类信息
        cd.fname = Classify.objects.get(id=cd.pid)
        # 查询当前分类的同级信息 ,查询pid和我的pid一样的分类信息,并排除我自己
        cd.siblings = Classify.objects.filter(pid=cd.pid).exclude(id=cd.id)

    # 一级分类
    # {
    #     'name':'服装',
    #     'sub':[
    #           {'name':'男装','sub':[{},{}]},
    #           {'name':'女装','sub':[{},{}]}
    #         ]
    # }

    # 二级分类
    # {
    #     'name':'男装',
    #     'sub':[{},{}],
    #     'fname':{'name':'服装',id:1},
    #     'siblings':[{'name':'女装'}]
    # }

    # 判断当前是否有fname属性
    # if hasattr(cd,'fname'):
    #     # 如果有fname 证明时二级类
    #     print(cd.fname.name)
    #     print('二级类')
    # else:
    #     # 如果没有fname 证明时一级类
    #     print(cd.name)
    #     print('一级类')

    context = {'cl': classlist(), 'cgdata': cd}
    return render(request, 'mall/shoplist.html', context)


def detail(request, gid):
    gs = Goods.objects.get(id=gid)
    return render(request, 'mall/detail.html', {'gs': gs})


# -------------cart-----------------------
def cart(request):
    # 获取购物车信息
    cart = request.session.get('cart', {})
    context = {'cart': cart}
    return render(request, 'mall/cart.html', context)


def cartadd(request):
    # 接受参数 商品id,购买数量
    gid = request.POST['gid']
    num = int(request.POST['num'])

    # 获取session中的购物车
    data = request.session.get('cart', {})

    # 判断是否有商品已经存在于购物车中
    if gid in data.keys():
        # 如果商品已经存在于购物车中,只修改数量
        data[gid]['num'] += num
    else:
        # 如果不存在,则查询数据,添加到购物车中
        # 获取商品对象
        goods = Goods.objects.get(id=gid)
        # 组装数据
        arr = {'gname': goods.gname, 'id': goods.id, 'price': float(goods.price), 'gpic': goods.gpic, 'num': num}
        data[gid] = arr

    # 存入session购物车中
    request.session['cart'] = data

    return HttpResponse('<script>alert("添加成功");location.href="/mall/cart/"</script>')
        # data=
        # {
        #     6: {'id': 6, 'price': 99.9, 'pic': '/static/public/img/1523839869.9352133.jpg', 'num': '1', 'title': '红虎皮超短裙,配黑丝袜'},
        #     5: {'id': 5, 'price': 199.8, 'pic': '/static/public/img/1523839666.313034.jpg', 'title': '倜傥风流的大鼻涕 风衣', 'num': '1'}
        # }


def cartdel(request):
    gid = request.GET['gid']
    cart = request.session['cart']

    del cart[gid]
    request.session['cart'] = cart
    return HttpResponse('')


def cartedit(request):
    gid = request.GET.get('gid', None)
    num = request.GET.get('num', None)

    cart = request.session.get('cart', {})
    cart[gid]['num'] = int(num)
    request.session['cart'] = cart

    return HttpResponse('')


# ---------登录注册-------------
def regist(request):
    return render(request, 'mall/register.html')


def doregist(request):
    vip = Users()
    vip.username = request.POST['username']
    vip.password = make_password(request.POST['password'], None, 'pbkdf2_sha256')
    vip.email = request.POST['email']
    # vip.tname = request.POST['tname']
    # vip.sex = request.POST['sex']
    # vip.phone = request.POST['phone']
    # vip.address = request.POST['address']
    # vip.state = request.POST['state']
    vip.pic = "/static/userpics/default.jpg"
    vip.save()
    request.session['UserLogin'] = {'uid': vip.id, 'uname': vip.username, 'upic': vip.pic}
    return HttpResponse('<script>alert("注册成功");location.href="/mall/";</script>')


def loginpage(request):
    return render(request, 'mall/login.html')


def userlogin(request):
    # 判断验证码是否正确
    # if request.POST['vcode'].upper() != request.session['verifycode']:
    # 	return HttpResponse('<script>alert("验证码错误");location.href="/myadmin/login"</script>')
    try:
        # 获取用户是否存在
        ob = Users.objects.get(username=request.POST['username'])
        # 判断状态是否为启用
        if ob.state == 1:
            # 验证密码
            res = check_password(request.POST['password'], ob.password)
            # 判断密码是否正确
            if res:
                # 登录成功,用户信息.,记录到session,跳转地址
                request.session['UserLogin'] = {'uid': ob.id, 'uname': ob.username, 'upic': ob.pic}
                return HttpResponse('<script>alert("登录成功");location.href="/mall/"</script>')
            else:
                # 密码错误
                # raise
                return HttpResponse('<script>alert("用户名或密码不正确");location.href="/mall/login/"</script>')
        else:
            # 不是启用状态
            # raise
            return HttpResponse('<script>alert("用户名或密码不正确");location.href="/mall/login/"</script>')
    except:
        # 用户不存在
        return HttpResponse('<script>alert("用户名或密码不正确");location.href="/mall/login/"</script>')


def userlogout(request):
    # 清除session登录信息
    request.session['UserLogin'] = {}
    return HttpResponse('<script>location.href="/mall/"</script>')


# --------------订单---------------
def confirm(request):
    # 选择购买的商品id
    ids = request.POST.get('ids', '').split(',')
    # # 购物车数据{6:{},5:{}}
    cart = request.session['cart']
    # 组装已购买的商品数据
    order = []
    for x in ids:
        order.append(cart[x])

    context = {'orders': order, 'ids': ids}
    return render(request, 'mall/order.html', context)


def createorder(request):
    # 购买的商品id [1,2,4]
    ids = request.POST.get('ids', '').split(',')
    # {1:{},2:{},4:{}}
    cart = request.session['cart']
    totalprice = 0
    totalnum = 0

    for x in ids:
        totalprice += (cart[x]['price'] * cart[x]['num'])
        totalnum += cart[x]['num']

    # 先去添加订单信息
    od = Order()
    od.uid = Users.objects.get(id=request.session['UserLogin']['uid'])
    # 收货信息 
    od.addname = request.POST['addname']
    od.address = request.POST['address']
    od.addphone = request.POST['addphone']
    od.addcode = request.POST['addcode']
    # 总价
    od.totalprice = totalprice
    # 总数
    od.totalnum = totalnum
    # 状态
    od.status = 1

    od.save()

    # 再去添加订单详情信息
    for x in ids:
        info = OrderInfo()
        info.orderid = od
        info.gid = Goods.objects.get(id=x)
        info.num = cart[x]['num']
        info.price = cart[x]['price']
        info.save()
        # 购物车中对应的商品信息
        del cart[x]

    # 把购物车信息重新存入session中
    request.session['cart'] = cart

    # 跳转到付款页面
    return HttpResponse('<script>location.href="/mall/gotopay/' + str(od.id) + '"</script>')


# 进入付款页面
def gotopay(request, oid):
    ob = Order.objects.get(id=oid)
    context = {'order': ob}
    return render(request, 'mall/pay.html', context)


# 完成支付
def pay(request, oid):
    ob = Order.objects.get(id=oid)
    ob.status = 2
    ob.save()
    return HttpResponse('<script>alert("付款成功");location.href="/mall/myorder/"</script>')


# 用户订单列表页
def myorder(request):
    # 获取用户id
    uid = request.session['UserLogin']['uid']
    # 查询用户的订单
    ob = Order.objects.filter(uid=uid)
    # filter 查询集 [{info:[{},{}]},{}]
    for x in ob:
        x.info = x.orderinfo_set.all()
    # print(ob[0].info[0].gid.title)

    dfk = Order.objects.filter(uid=uid, status=1)
    for x in dfk:
        x.info = x.orderinfo_set.all()

    dfh = Order.objects.filter(uid=uid, status=2)
    for x in dfh:
        x.info = x.orderinfo_set.all()

    yfh = Order.objects.filter(uid=uid, status=3)
    for x in yfh:
        x.info = x.orderinfo_set.all()

    return render(request, 'mall/myorder.html', {'orders': ob, 'daifukuan': dfk, 'daifahuo': dfh, 'yifahuo': yfh})


# 取消订单
def cancelorder(request, oid):
    ob = Order.objects.get(id=oid)
    ob.status = 5
    ob.save()
    return HttpResponse('<script>alert("订单已取消");location.href="/mall/myorder/"</script>')


# 确认收货
def orderok(request, oid):
    ob = Order.objects.get(id=oid)
    ob.status = 4
    ob.save()
    return HttpResponse('<script>alert("订单完成");location.href="/mall/myorder/"</script>')


def member(request):
    if request.session.get('UserLogin',None):
        uid = request.session['UserLogin']['uid']
        ob = Users.objects.get(id=uid)
        od1 = Order.objects.filter(uid=uid, status=1)
        od2 = Order.objects.filter(uid=uid, status=3)
        ob.dfk = len(od1)
        ob.dsh = len(od2)

        return render(request, 'mall/member.html', {'vip': ob})
    else:
        return HttpResponse('<script>alert("请先登录!");location.href="/mall/login/"</script>')


def myorderinfo(request, oid):
    ob = Order.objects.get(id=oid)
    ob.info = ob.orderinfo_set.all()
    return render(request, 'mall/myorderinfo.html', {'order': ob})


def myinfo(request):
    uid = request.session['UserLogin']['uid']
    ob = Users.objects.get(id=uid)
    return render(request, 'mall/myinfo.html', {'vip': ob})


def updatemyinfo(request):
    try:
        vip = Users.objects.get(id=request.POST['uid'])
        vip.username = request.POST['username']
        vip.email = request.POST['email']
        vip.tname = request.POST['tname']
        vip.sex = request.POST['sex']
        vip.phone = request.POST['phone']
        vip.address = request.POST['address']

        newpic = request.FILES.get('pic', None)
        if newpic:
            picname = str(time.time()) + "." + newpic.name.split('.').pop()
            destination = open("./static/userpics/" + picname, "wb+")
            for chunk in newpic.chunks():  # 分块写入文件  
                destination.write(chunk)
            destination.close()

            if vip.pic != "/static/userpics/default.jpg":
                os.remove('.' + vip.pic)
            vip.pic = '/static/userpics/' + picname
        vip.save()
        return HttpResponse('<script>alert("个人信息已保存");location.href="/mall/member/";</script>')
    except:
        return HttpResponse('<script>alert("修改失败");location.href="/mall/myinfo/";</script>')



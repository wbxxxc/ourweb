from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^$', views.mallindex, name='mallindex'),
    url(r'^shoplist/(?P<cid>[0-9]+)', views.shoplist, name='shoplist'),
    url(r'^detail/(?P<gid>[0-9]+)', views.detail, name='detail'),
    # ---------登录注册--------------
    url(r'^regist/', views.regist, name='regist'),
    url(r'^doregist/', views.doregist, name='doregist'),
    url(r'^login/', views.loginpage, name='loginpage'),
    url(r'^userlogin/', views.userlogin, name='userlogin'),
    url(r'^userlogout/', views.userlogout, name='userlogout'),
    # ----------cart----------------
    url(r'^cart/', views.cart, name='cart'),
    url(r'^cartadd/', views.cartadd, name='cartadd'),
    url(r'^cartdel/', views.cartdel, name='cartdel'),
    url(r'^cartedit/', views.cartedit, name='cartedit'),
    # ----------订单，需要登录------------------
    url(r'^member/', views.member, name="member"),
    # 提交订单,确认订单信息
    url(r'^confirm/', views.confirm, name="confirm"),
    # 生成订单
    url(r'^createorder/', views.createorder, name="createorder"),
    # 去付款页面
    url(r'^gotopay/(?P<oid>[0-9]+)', views.gotopay, name="gotopay"),
    # 付款操作
    url(r'^pay/(?P<oid>[0-9]+)', views.pay, name="pay"),
    # 我的订单
    url(r'^myorder/', views.myorder, name="myorder"),
    # 个人信息
    url(r'^myinfo/', views.myinfo, name="myinfo"),
    # 个人信息修改
    url(r'^updatemyinfo/', views.updatemyinfo, name="updatemyinfo"),
    # 订单详情
    url(r'^myorderinfo/(?P<oid>[0-9]+)', views.myorderinfo, name="myorderinfo"),
    # 取消订单
    url(r'^cancelorder/(?P<oid>[0-9]+)', views.cancelorder, name="cancelorder"),
    # 确认收货
    url(r'^orderok/(?P<oid>[0-9]+)', views.orderok, name="orderok"),

]

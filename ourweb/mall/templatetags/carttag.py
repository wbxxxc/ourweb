from django import template
register = template.Library()

# 自定义乘法函数
@register.simple_tag
def cheng(var1,var2):
    res = float(var1) * float(var2)
    return '%.2f'%res